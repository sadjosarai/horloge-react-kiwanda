import React from 'react';
import logo from './logo.svg';
import moment from "moment";
import './App.css';
//import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Icon from '@material-ui/core/Icon';

const horloge="https://lh3.googleusercontent.com/GxHSwhFoDM4kVhEiZqbaFKqfXYw-6gGoax13vTE2wCFFFJI-izlKvSvvGs72vzYBbnJcyxx_7gM2s0s=w1903-h937";


class Clock extends React.Component {
  constructor (props){
    super(props);
    this.state={date:new Date()};
  }
  componentDidMount(){
    this.timerID=setInterval(
      ()=>this.tick(),1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick(){
    this.setState({
      date:new Date()
    });
  }

  render(){
    return (
      <div id="clock-container">
        <div id="horloge">
          <h1 id="day">{moment(this.state.date).format("dddd")}</h1>
          <h2 id="moment">this will befor the moment of the day.</h2>
          <h1 id="hours">{this.state.date.toLocaleTimeString()}.</h1>
          <h2 id="date">{moment(this.state.date).format("Do MMMM YYYY")}.</h2>
        </div>
      </div>
    );
  }
}


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Welcome, this is the asignment of building a clock looking like this :<br/>
          <img src={horloge} className="sample" alt="clockImage" />
        </p>
        <p>
          You can see my work below and by clicking on one of the links below you can see my other asignments.
        </p>
        <nav className="navbar">
       
        <a
          className="App-link text-left"
          href="https://kiwanda-hello-world.web.app"
          target="_blank"
          rel="noopener noreferrer"
        >
           <Icon>keyboard_return</Icon> Hello-world
        </a>
        <a
          className="App-link text-right"
          href="https://kiwanda-hello-world.web.app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Todo List <Icon>shortcut</Icon>
        </a>
        </nav>

      </header>
      <footer className="App-footer">
        <Clock />
      </footer>
    </div>
  );
}

export default App;